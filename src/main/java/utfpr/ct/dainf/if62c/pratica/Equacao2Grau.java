/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;
/**
 *
 * @author nmessias
 * @param <T>
 */
public class Equacao2Grau<T extends Number> {
    private T a, b, c;
    
    public Equacao2Grau(T a, T b, T c){
        if(isZero(a))
            throw new RuntimeException(String.format("Coeficiente %c não pode ser zero", 'a'));
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * @return the a
     */
    public T getA() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void setA(T a) {
        if(isZero(a))
            throw new RuntimeException(String.format("Coeficiente %c não pode ser zero", 'a'));
        this.a = a;
    }

    /**
     * @return the b
     */
    public T getB() {
        return b;
    }

    /**
     * @param b the b to set
     */
    public void setB(T b) {
        this.b = b;
    }

    /**
     * @return the c
     */
    public T getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(T c) {
        this.c = c;
    }
    
    private double getDelta(){
        return Math.pow(b.doubleValue(), 2.0) - (4.0 * a.doubleValue() * c.doubleValue());
    }
    
    public double getRaiz1(){
        if(getDelta() < 0.0)
            throw new RuntimeException("Equação não tem solução real");
        double raiz = (-b.doubleValue() - Math.sqrt(getDelta())) / 2 * a.doubleValue();
        return raiz;
    }
    
    public double getRaiz2(){
        if(getDelta() < 0.0)
            throw new RuntimeException("Equação não tem solução real");
        double raiz = (-b.doubleValue() + Math.sqrt(getDelta())) / 2 * a.doubleValue() ;
        return raiz;
    }
    
    private boolean isZero(T number){
        return (Math.abs(number.doubleValue()) < 2 * Double.MIN_VALUE);
    }
    
}
