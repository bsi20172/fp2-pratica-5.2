/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */

import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

public class Pratica52 {
    public static void main(String[] args) {
        try{
        Equacao2Grau eq = new Equacao2Grau(3,-7,4);
        System.out.println(eq.getRaiz1());
        System.out.println(eq.getRaiz2());
        eq.setA(9);
        eq.setB(-12);
        System.out.println(eq.getRaiz1());
        System.out.println(eq.getRaiz2());
        eq.setA(5);
        eq.setB(3);
        eq.setC(5);
        System.out.println(eq.getRaiz1());
        System.out.println(eq.getRaiz2());
        }
        catch(RuntimeException e){
            System.out.println(e);
        }
    }
}
